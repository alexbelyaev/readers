#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 17:49:31 2022

@author: alex-stend
"""
# ======================================================================================================
####    Libs import
# ======================================================================================================
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import constants
from scipy import signal
# import plotly
# import plotly.graph_objects as go
#import cv2 as cv
import proc_funcs as af
from scipy.fftpack import fft
import time
import scipy.io as sio
# import applied_math as am
# ======================================================================================================


# non-interactive backend
# mpl.use('agg')
mpl.use('Qt5Agg')

# ======================================================================================================
####    Principal Constants
# ======================================================================================================
pi = np.pi
gr = pi/180.0
c  = constants.speed_of_light   # (m/s)
i  = 1j
# ======================================================================================================


#=======================================================================================================
####    File routes
#=======================================================================================================
base_route = '/home/alex-stend/datasets/raw_data_17_02_2022/'
# file_cfg   = base_route + 'config_32x256.xml'
file_bin   = base_route + 'normal_radar_one_corner_test.bin'
one_corner_set = []

# file_bin   = base_route + 'normal_radar_two_corners_test.bin'
# two_corners_set = []


# file_bin   = 'RV_RA_model_data_2obj.bin'

#=======================================================================================================


#=======================================================================================================
####    Radar Parameters
#=======================================================================================================
# smp_num, fd, S, fc, ti, tr, pls_num, frames, tx_phys = af.read_cfg_file(file_cfg)
SAMPLE_MAX = 8000

smp_num = 256
fd = 18.75e6
S  = 90e12
fc = 77.5e9
ti = 5.0e-6            # idle time
tr = 20.0e-6           # ramp end time

####    general physical and data parameters
sbfrs  = 3             # number of subframes
lmb    = c/fc          # wavelength, m
k      = 2*pi/lmb      # wavenumber, rad/m
smp_w  = 2             # bytes per a sample
d      = lmb/2.0       # antenna spacing, m
tx_azm = 4             # number of tx for azm MIMO subframe
tx_elv = 5             # number of tx for elv MIMO subframe
tx_vel = 1             # number of tx for vel subframe

ts = smp_num / fd      # ADC sampling time
fb = S * ts            # bandwidth
tp = ti + tr           # pulse width

pls_num = 64           # MIMO (azm, elv) pulses number
pls_num_vf = 128       # pulses number for velocity frame

# zero padding
pls_pad = 256
smp_pad = 512
azm_pad = 256
elv_pad = 128
pls_pad_vf = 256
pls_acc = pls_num

rx_virt_a = 64                  # virtual antennas number azm MIMO
rx_virt_e = 5                   # virtual antennas number elv MIMO

# DC component cuttof
r_dec_0 = int(2)      #int(0.02*smp_pad)
r_dec_m = smp_pad - 2 #int(0.95*smp_pad)
# Azimuth negligible bins (by sides)
a_dec   = int(8)

# coordinates scales
f_smp = np.zeros(smp_pad, float)
r_mas = np.zeros(smp_pad, float)
a_mas = np.zeros(azm_pad, float)
e_mas = np.zeros(elv_pad, float)
v_mas = np.zeros(pls_pad, float)
v_mas_vf = np.zeros(pls_pad_vf, float)


for j_smp in range(0, smp_pad):
    f_smp[j_smp] = j_smp/(ts*(smp_pad/smp_num))
r_mas = f_smp*c/(2*S)
dr = r_mas[smp_pad-1]/smp_pad

for j_ps in range(0, pls_pad):
    v_mas[j_ps] = (j_ps - (pls_pad >> 1))*c / (pls_pad*tp*tx_azm*2*fc)
dv = (v_mas[pls_pad-1] - v_mas[0]) / pls_pad

for j_rx in range(0, azm_pad):
    k_smp = (j_rx - (azm_pad>>1)) / azm_pad
    a_mas[j_rx] = (1/gr)*np.arcsin(k_smp*lmb/d)

for j_rx in range(0, elv_pad):
    k_smp = (j_rx - (elv_pad>>1)) / elv_pad
    e_mas[j_rx] = (1/gr)*np.arcsin(k_smp*lmb/d)
    
for j_ps in range(0, pls_pad_vf):
    v_mas_vf[j_ps] = (j_ps - (pls_pad_vf >> 1))*c / (pls_pad_vf*tp*tx_vel*2*fc)
dv_vf = (v_mas_vf[pls_pad-1] - v_mas_vf[0]) / pls_pad_vf

# fft windowing
hamm_win_V = af.hamm_win(pls_num)
hamm_win_R = af.hamm_win(smp_num)
hamm_win_A = af.hamm_win(rx_virt_a)
hamm_win_V_vf = af.hamm_win(pls_num_vf)

#=======================================================================================================


#=======================================================================================================
####    System parameters
#=======================================================================================================
# Maximum range
D_max = (c*fd) / (2*S)
print("\nMaximum range D_max = %.1f" % D_max, "m")
# Range resolution
d_D = c /(2*fb)
print("Range resolution d_D = %.3f" % d_D, "м")

# Maximum velocity for azm MIMO subframe
v_max = lmb / (4*tp*tx_azm)
print("Maximum velocity (azm MIMO) v_max = %.3f" % v_max, "m/s", end = " ")
print("(%.3f" % (v_max*3.6), " km/h)")
# Velocity resolution for azm MIMO subframe
d_v = 2*v_max / pls_num
print("Velocity resolution (azm MIMO) d_v = %.3f" % d_v, "m/s", end = " ")
print("(%.3f" % (d_v*3.6), " km/h)")

# Maximum velocity for vel subframe
v_max = lmb / (4*tp*tx_vel)
print("Maximum velocity (vel subframe) v_max = %.3f" % v_max, "m/s", end = " ")
print("(%.3f" % (v_max*3.6), " km/h)")
# Velocity resolution for vel subframe
d_v = 2*v_max / pls_num_vf
print("Velocity resolution (vel subframe) d_v = %.3f" % d_v, "m/s", end = " ")
print("(%.3f" % (d_v*3.6), " km/h)")

# Azimuth Resolution
d_azm = 360 / (rx_virt_a*pi)
print("Azimuth resolution d_azm = %.3f" % d_azm, "grad")

# Elevation Resolution
d_elv = 360 / (rx_virt_e*pi)
print("Elevation resolution d_elv = %.3f" % d_elv, "grad")

#=======================================================================================================
####     Radar data reading
#=======================================================================================================
print('\n')

# number of samples per frame
chp_num = 4        # AWR chips number in cascade
rx_phys = 4        # Rx number for one AWR
# number of complex samples per MIMO azm subframe
frm_azm_cs = chp_num*rx_phys*tx_azm*smp_num*pls_num
# number of complex samples per MIMO elv subframe
frm_elv_cs = chp_num*rx_phys*tx_elv*smp_num*pls_num
# number of complex samples per vel subframe
frm_vel_cs = chp_num*rx_phys*tx_vel*smp_num*pls_num_vf
# number of samples per frame (factor 2 - for complex samples)
frm_s  = 2*(frm_azm_cs + frm_elv_cs + frm_vel_cs)
# Frame size in bytes (factor 2 - for 16 bit samples)
frm_w  = 2*frm_s

chp_cs = rx_phys*tx_azm*smp_num*pls_num + rx_phys*tx_elv*smp_num*pls_num + rx_phys*tx_vel*smp_num*pls_num_vf

frame = 0


for frame in range(0, 20):
    print('frame under processing = ', frame)
    
    data = open(file_bin, 'rb')
    position = frame*frm_w
    data.seek(position, 0)
    Xt = np.fromfile(data, dtype = np.dtype('int16'), count = frm_s)
        
    Xt_I = Xt[::2]
    Xt_Q = Xt[1::2]
    Xt = Xt_I + 1j*Xt_Q
    
    Xtg = (Xt[         : chp_cs])
    Xtb = (Xt[  chp_cs : 2*chp_cs])
    Xtm = (Xt[2*chp_cs : 3*chp_cs])
    Xtu = (Xt[3*chp_cs : 4*chp_cs])

    # Azimut subframe
    chip_azm_cs = rx_phys*tx_azm*smp_num*pls_num   # one chip samples number (MIMO azm)
    Xtb_azm = np.reshape(Xtb[ : chip_azm_cs], (pls_num, tx_azm, rx_phys, smp_num))
    Xtg_azm = np.reshape(Xtg[ : chip_azm_cs], (pls_num, tx_azm, rx_phys, smp_num))
    Xtm_azm = np.reshape(Xtm[ : chip_azm_cs], (pls_num, tx_azm, rx_phys, smp_num))
    Xtu_azm = np.reshape(Xtu[ : chip_azm_cs], (pls_num, tx_azm, rx_phys, smp_num))
    
    # Elevation subframe
    elv_start = chip_azm_cs
    chip_elv_cs = rx_phys*tx_elv*smp_num*pls_num   # one chip samples number (MIMO elv)
    elv_end   = chip_azm_cs + chip_elv_cs
    Xtb_elv = np.reshape(Xtb[elv_start : elv_end], (pls_num, tx_elv, rx_phys, smp_num))
    Xtg_elv = np.reshape(Xtg[elv_start : elv_end], (pls_num, tx_elv, rx_phys, smp_num))
    Xtm_elv = np.reshape(Xtm[elv_start : elv_end], (pls_num, tx_elv, rx_phys, smp_num))
    Xtu_elv = np.reshape(Xtu[elv_start : elv_end], (pls_num, tx_elv, rx_phys, smp_num))
    
    # Velocity subframe
    vel_start = elv_end
    chip_vel_cs = rx_phys*tx_vel*smp_num*pls_num_vf   # one chip samples number (vel subframe)
    vel_end = vel_start + chip_vel_cs
    Xtb_vel = np.reshape(Xtb[vel_start : vel_end], (pls_num_vf, tx_vel, rx_phys, smp_num))
    Xtg_vel = np.reshape(Xtg[vel_start : vel_end], (pls_num_vf, tx_vel, rx_phys, smp_num))
    Xtm_vel = np.reshape(Xtm[vel_start : vel_end], (pls_num_vf, tx_vel, rx_phys, smp_num))
    Xtu_vel = np.reshape(Xtu[vel_start : vel_end], (pls_num_vf, tx_vel, rx_phys, smp_num))
 
    # Xt_azm = (Xt[: frm_azm_cs])             # MIMO azm subframe data
    # vel_s = frm_azm_cs + frm_vel_cs
    # Xt_vel = (Xt[frm_azm_cs : vel_s])       # velocity subframe data
    # elv_s = vel_s + frm_elv_cs
    # Xt_elv = (Xt[vel_s : elv_s])            # MIMO elv subframe data
    
    # # Azimut subframe
    # chip_azm_cs = rx_phys*tx_azm*smp_num*pls_num   # one chip samples number (MIMO azm)
    # Xtb_azm = np.reshape(Xt_azm[             :  chip_azm_cs], (pls_num, tx_azm, rx_phys, smp_num))
    # Xtg_azm = np.reshape(Xt_azm[chip_azm_cs  :2*chip_azm_cs], (pls_num, tx_azm, rx_phys, smp_num))
    # Xtm_azm = np.reshape(Xt_azm[2*chip_azm_cs:3*chip_azm_cs], (pls_num, tx_azm, rx_phys, smp_num))
    # Xtu_azm = np.reshape(Xt_azm[3*chip_azm_cs:4*chip_azm_cs], (pls_num, tx_azm, rx_phys, smp_num))
    
    # # Elevation subframe
    # chip_elv_cs = rx_phys*tx_elv*smp_num*pls_num   # one chip samples number (MIMO elv)
    # Xtb_elv = np.reshape(Xt_elv[             :  chip_elv_cs], (pls_num, tx_elv, rx_phys, smp_num))
    # Xtg_elv = np.reshape(Xt_elv[  chip_elv_cs:2*chip_elv_cs], (pls_num, tx_elv, rx_phys, smp_num))
    # Xtm_elv = np.reshape(Xt_elv[2*chip_elv_cs:3*chip_elv_cs], (pls_num, tx_elv, rx_phys, smp_num))
    # Xtu_elv = np.reshape(Xt_elv[3*chip_elv_cs:4*chip_elv_cs], (pls_num, tx_elv, rx_phys, smp_num))
    
    # # Velocity subframe
    # chip_vel_cs = rx_phys*tx_vel*smp_num*pls_num_vf   # one chip samples number (vel subframe)
    # Xtb_vel = np.reshape(Xt_vel[             :  chip_vel_cs], (pls_num_vf, tx_vel, rx_phys, smp_num))
    # Xtg_vel = np.reshape(Xt_vel[  chip_vel_cs:2*chip_vel_cs], (pls_num_vf, tx_vel, rx_phys, smp_num))
    # Xtm_vel = np.reshape(Xt_vel[2*chip_vel_cs:3*chip_vel_cs], (pls_num_vf, tx_vel, rx_phys, smp_num))
    # Xtu_vel = np.reshape(Xt_vel[3*chip_vel_cs:4*chip_vel_cs], (pls_num_vf, tx_vel, rx_phys, smp_num))
    
#=======================================================================================================
    # Azm 64 elements
    Xtb16 = np.concatenate((Xtb_azm[:, 0, :, :], Xtb_azm[:, 1, :, :], 
                            Xtb_azm[:, 2, :, :], Xtb_azm[:, 3, :, :]), axis=1)
    Xtm16 = np.concatenate((Xtm_azm[:, 0, :, :], Xtm_azm[:, 1, :, :], 
                            Xtm_azm[:, 2, :, :], Xtm_azm[:, 3, :, :]), axis=1)
    Xtu16 = np.concatenate((Xtu_azm[:, 0, :, :], Xtu_azm[:, 1, :, :], 
                            Xtu_azm[:, 2, :, :], Xtu_azm[:, 3, :, :]), axis=1)
    Xtg16 = np.concatenate((Xtg_azm[:, 0, :, :], Xtg_azm[:, 1, :, :], 
                            Xtg_azm[:, 2, :, :], Xtg_azm[:, 3, :, :]), axis=1)

    Xt_azm = np.concatenate((Xtb16, Xtg16, Xtm16, Xtu16), axis=1)
    
#=======================================================================================================
    # Elv 5 elements
    Xt_elv = np.concatenate(( Xtg_elv[:, 0, 2:3, :],
                              Xtu_elv[:, 1, 0:1, :],
                              Xtu_elv[:, 2, 2:3, :],
                              Xtm_elv[:, 3, 2:3, :],
                              Xtm_elv[:, 4, 0:1, :]),
                            axis=1)

#=======================================================================================================
    # Vel 16 elements
    Xt_vel = np.concatenate((Xtb_vel[:, 0, :, :], Xtg_vel[:, 0, :, :], 
                             Xtm_vel[:, 0, :, :], Xtu_vel[:, 0, :, :]), axis=1)
    
#=======================================================================================================
    dev_pls = np.zeros(pls_num, float)
    for tx_ref in range(0, tx_azm):
        for rx_ref in range(0, rx_phys):
            
            for pls_ref in range(0, pls_num):
                max_re = max(Xtb_azm[pls_ref, tx_ref, rx_ref, :].real)
                max_im = max(Xtb_azm[pls_ref, tx_ref, rx_ref, :].imag)
                dev_pls[pls_ref] = (max_re**2 + max_im**2)**0.5
                if (abs(max_re) > SAMPLE_MAX or abs(max_im) > SAMPLE_MAX):
                    print('bad chip:  tx:',  tx_ref, ';  rx:',   rx_ref, ';  pls:', pls_ref)
                
            # plt.figure(1).patch.set_facecolor('xkcd:lavender') # 'xkcd:pale green'  # xkcd:mauve
            # plt.plot(dev_pls)
            # plt.grid(color='g', linestyle='--', linewidth=1)
    
            for pls_ref in range(0, pls_num):
                max_re = max(Xtg_azm[pls_ref, tx_ref, rx_ref, :].real)
                max_im = max(Xtg_azm[pls_ref, tx_ref, rx_ref, :].imag)
                dev_pls[pls_ref] = (max_re**2 + max_im**2)**0.5
                if (abs(max_re) > SAMPLE_MAX or abs(max_im) > SAMPLE_MAX):
                    print('good chip:  tx:',  tx_ref, ';  rx:',   rx_ref, ';  pls:', pls_ref)
                
            # plt.figure(2).patch.set_facecolor('xkcd:lavender') # 'xkcd:pale green'  # xkcd:mauve
            # plt.plot(dev_pls)
            # plt.grid(color='g', linestyle='--', linewidth=1)

            for pls_ref in range(0, pls_num):
                max_re = max(Xtm_azm[pls_ref, tx_ref, rx_ref, :].real)
                max_im = max(Xtm_azm[pls_ref, tx_ref, rx_ref, :].imag)
                dev_pls[pls_ref] = (max_re**2 + max_im**2)**0.5
                if (abs(max_re) > SAMPLE_MAX or abs(max_im) > SAMPLE_MAX):
                    print('master chip:  tx:',  tx_ref, ';  rx:',   rx_ref, ';  pls:', pls_ref)
                
            # plt.figure(3).patch.set_facecolor('xkcd:lavender') # 'xkcd:pale green'  # xkcd:mauve
            # plt.plot(dev_pls)
            # plt.grid(color='g', linestyle='--', linewidth=1)
            
            for pls_ref in range(0, pls_num):
                max_re = max(Xtu_azm[pls_ref, tx_ref, rx_ref, :].real)
                max_im = max(Xtu_azm[pls_ref, tx_ref, rx_ref, :].imag)
                dev_pls[pls_ref] = (max_re**2 + max_im**2)**0.5
                if (abs(max_re) > SAMPLE_MAX or abs(max_im) > SAMPLE_MAX):
                    print('ugly chip:  tx:',  tx_ref, ';  rx:',   rx_ref, ';  pls:', pls_ref)
                
            # plt.figure(4).patch.set_facecolor('xkcd:lavender') # 'xkcd:pale green'  # xkcd:mauve
            # plt.plot(dev_pls)
            # plt.grid(color='g', linestyle='--', linewidth=1)

    rx = 3
    tx = 1
    pls = 28
    # plt.figure(10).patch.set_facecolor('xkcd:lavender') # 'xkcd:pale green'  # xkcd:mauve
    # plt.plot(Xtm_azm[pls, tx, rx, :].real, 'm-')
    # plt.plot(Xtm_azm[pls, tx, rx, :].imag, 'c-')
    # plt.grid(color='g', linestyle='--', linewidth=1)

#=======================================================================================================
####    fft
#=======================================================================================================
# Azimuth MIMO subframe
    asd_Ra  = fft(Xt_azm*hamm_win_R[None, None, :], n=smp_pad, axis=2)
    asd_RVa = fft(asd_Ra*hamm_win_V[:, None, None], n=pls_pad, axis=0)
    psd_RVa = asd_RVa.real**2 + asd_RVa.imag**2
    psd_RV_azm = np.sum(psd_RVa, axis=1)
    psd_RV_azm = np.fft.fftshift(psd_RV_azm, 0)

    psd_R_azm = psd_RV_azm[pls_pad>>1, :]
    
    # psd_RV_azm[:, :r_dec_0] = 0
    # psd_RV_azm[:, r_dec_m:smp_pad] = 0
    # psd_RV_azm /= np.max(psd_RV_azm)
    
    asd_RVA = fft(asd_RVa*hamm_win_A[None, :, None], n=azm_pad, axis=1)
    asd_RA  = fft(asd_Ra *hamm_win_A[None, :, None], n=azm_pad, axis=1)
    
    psd_RA = asd_RA.real**2 + asd_RA.imag**2
    psd_RA_acc = np.sum(psd_RA[:pls_acc, :, :], axis=0)
    psd_RA_acc = np.fft.fftshift(psd_RA_acc, 0)
    psd_RA_acc[:, :r_dec_0] = 0
    psd_RA_acc[:, r_dec_m+1:smp_pad] = 0
    # psd_RA_acc /= np.max(psd_RA_acc)

    psd_RVA = asd_RVA.real**2 + asd_RVA.imag**2
    psd_RVA = np.fft.fftshift(psd_RVA, 0)
    psd_RVA = np.fft.fftshift(psd_RVA, 1)
    
    psd_R_azm_ = np.sum(psd_RVA[pls_pad>>1, :, :], 0)
    
    psd_RVA_acc = np.sum(psd_RVA, axis=0)
    psd_RVA_acc[:, :r_dec_0] = 0
    psd_RVA_acc[:, r_dec_m+1:smp_pad] = 0
    # psd_RVA_acc /= np.max(psd_RVA_acc)   

# Elevation MIMO subframe
    asd_Re  = fft(Xt_elv*hamm_win_R[None, None, :], n=smp_pad, axis=2)
    asd_RVe = fft(asd_Re*hamm_win_V[:, None, None], n=pls_pad, axis=0)
    asd_RVE = fft(asd_RVe, n=elv_pad, axis=1)

    psd_RVe = asd_RVe.real**2 + asd_RVe.imag**2
    psd_RV_elv = np.sum(psd_RVe, axis=1)
    psd_RV_elv = np.fft.fftshift(psd_RV_elv, 0)

    psd_RV_elv[:, :r_dec_0] = 0
    psd_RV_elv[:, r_dec_m:smp_pad] = 0
    # psd_RV_elv /= np.max(psd_RV_elv)

    psd_RVE = asd_RVE.real**2 + asd_RVE.imag**2
    psd_RVE = np.sum(psd_RVE, axis=0)
    psd_RVE = np.fft.fftshift(psd_RVE, 0)

    psd_RVE[:, :r_dec_0] = 0
    psd_RVE[:, r_dec_m:smp_pad] = 0
    # psd_RVE /= np.max(psd_RVE)

#=======================================================================================================
####    
#=======================================================================================================

    # nonlinear R-profile
    r_prof_max = np.zeros(smp_pad, float)
    for ref in range(0, smp_pad):
        r_prof_max[ref] = np.max(psd_RVA[:, :, ref])


    # plt.figure(5).patch.set_facecolor('xkcd:pale green') # 'xkcd:pale green'  # xkcd:mauve
    # plt.plot(r_prof_max, 'b-')
    # plt.title(file_bin)
    # plt.grid(color='g', linestyle='--', linewidth=1)

    # # rbin = 258;    vbin =  93;    abin =  64

    # plt.figure(6).patch.set_facecolor('xkcd:pale green') # 'xkcd:pale green'  # xkcd:mauve
    # # plt.plot(psd_RVA[:, a_bin ,r_bin] / max(psd_RVA[:, a_bin ,r_bin]), 'b-')
    # plt.plot(psd_R_azm_ / max(psd_R_azm_), 'b-')
    # plt.grid(color='g', linestyle='--', linewidth=1)
    
    # # plt.figure().patch.set_facecolor('xkcd:pale green') # 'xkcd:pale green'  # xkcd:mauve
    # # plt.plot(human_factor, 'b-')
    # # plt.stem(pntsRVA[:, 0], ra_map[pntsRVA[:, 2], pntsRVA[:, 0]], 'r--')
    # # plt.grid(color='g', linestyle='--', linewidth=1)

    # # plt.figure().patch.set_facecolor('xkcd:lavender') # 'xkcd:pale green'  # xkcd:mauve
    # # plt.imshow(psd_RV_azm, vmax = 0.05*np.max(psd_RV_azm))

    
    # plt.figure().patch.set_facecolor('xkcd:pale green') # 'xkcd:pale green'  # xkcd:mauve
    # plt.plot(psd_RV_azm[pls_pad>>1, :])
    # # plt.title(file_bin)
    # plt.grid(color='g', linestyle='--', linewidth=1)

    plt.figure()
    plt.imshow(psd_RV_azm)
    # plt.xlim([66, 68])
    # plt.ylim([127, 129])
    
    one_corner_set.append(psd_RV_azm[128, 67]/1e15)
    # two_corners_set.append(psd_RV_azm[128, 67]/1e15)

    # break

data.close()

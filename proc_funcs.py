# -*- coding: utf-8 -*-
"""
Created on Mon Sep 16 13:51:42 2019

@author: suanov
"""


import numpy as np
import sys
import xml.etree.cElementTree as ET
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import signal
from scipy.fftpack import fft
#=======================================================================================================
#
#=======================================================================================================
# Нахождение ближайшего значения в массиве
# find_nearest(array, value):
# array - входной массив
# value - значение, ближайшее к которому необходимо найти
# Возвращает:
# idx - индекс ближайшего значения
#=======================================================================================================
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx
#=======================================================================================================


#=======================================================================================================
# Функции поиска максимумов и подавления боковых лепестков
# prof  - профиль карты
# stump - число неучитываемых отсчётов (по краям)
def maxima_finder(prof, stump):
    rxq_pad = len(prof)
    # производная профиля
    prof_d = prof.copy()
    prof_d[1:] -= prof[0:rxq_pad-1]
    prof_d[0] = prof_d[1]
    # максимумы профиля
    prof_max = []
    max_number = 0  # число максимумов в профиле
    for ref in range(stump, rxq_pad-1-stump):
        if ( (prof_d[ref] >= 0) and (prof_d[ref+1] <= 0) ):
            prof_max.append(ref)
            max_number += 1

    if len(prof_max) == 0:
        return prof_max

    # глобальный максимум профиля
    glob_max_value = prof[prof_max[0]]  # значние профиля в точке глобального максимума
    glob_max_posit = prof_max[0]           # положение главного максимума
    glob_max_index = 0                     # индекс главного максимума в массиве prof_max
    for ref in range(1, max_number):
        cur_max = prof_max[ref]
        if (prof[cur_max] > glob_max_value):
            glob_max_value = prof[cur_max]
            glob_max_posit = cur_max
            glob_max_index = ref

    # перестановка главного максимума в начало массива
    temp = prof_max[0]
    prof_max[0] = glob_max_posit
    prof_max[glob_max_index] = temp

    # устранение боковых лепестков
    ref = 1
    while (ref < max_number):
        cur_max = prof_max[ref]
        cur_max_value = prof[cur_max]
        max_dist = abs(glob_max_posit - cur_max) % (rxq_pad>>1)
        if (max_dist < (rxq_pad>>3)):
            if (cur_max_value < 0.5*glob_max_value):
                prof_max.pop(ref)
                max_number -= 1
                ref -=1
        elif (max_dist < (rxq_pad>>2)):
            if (cur_max_value < 0.5*glob_max_value):
                prof_max.pop(ref)
                max_number -= 1
                ref -=1
        elif (max_dist < (rxq_pad>>1)):
            if (cur_max_value < 0.5*glob_max_value):
                prof_max.pop(ref)
                max_number -= 1
                ref -=1
        ref += 1

    return prof_max
#=======================================================================================================


#=======================================================================================================
def median_maxima_finder(prof, max_lvl, dec):
    rxq_pad = len(prof)
    profile_integral = np.sum(prof[dec:rxq_pad-dec])
    profile_max_lvl = profile_integral*max_lvl/float(rxq_pad - 2*dec)
    # производная профиля
    prof_d = prof.copy()
    prof_d[1:] -= prof[0:rxq_pad-1]
    prof_d[0] = prof_d[1]

    # максимумы профиля
    prof_max = []
    max_number = 0  # число максимумов в профиле
    for ref in range(dec, rxq_pad-dec):
        if ( (prof_d[ref] >= 0) and (prof_d[ref+1] <= 0) ):
            if (prof[ref] > profile_max_lvl):
                prof_max.append(ref)
                max_number += 1

    return prof_max
#=======================================================================================================


#=======================================================================================================
# Окно Хэмминга
def hamm_win(smp_num):
    smp_values = np.zeros(smp_num, float)
    arg_shift = (smp_num-1)/2
    for ref in range(0, smp_num):
        smp_values[ref] = 0.54 + 0.46*np.cos(2*np.pi*(ref - arg_shift)/(smp_num-1))
    return smp_values

# Окно Ханна
def hann_win(smp_num):
    smp_values = np.zeros(smp_num, float)
    arg_shift = (smp_num-1)/2
    for ref in range(0, smp_num):
        smp_values[ref] = 0.5 + 0.5*np.cos(2*np.pi*(ref - arg_shift)/(smp_num-1))
    return smp_values
#=======================================================================================================


#=======================================================================================================
# чтение конфигурационного файла
def read_cfg_file(file_cfg):
    try:
        tree = ET.ElementTree(file = file_cfg) # Считывание xml файла
        root = tree.getroot() # Доступ к корневым параметрам
        # Считывание параметров в конфигурации профиля
        for elem in root.iterfind('apiname_profile_cfg/'):
            name = elem.get('name')
            value = float(elem.get('value').replace(',', '.'))
            if name == 'numAdcSamples':
                smp_num = int(elem.get('value').replace(',', '.'))
            if name == 'digOutSampleRate':
                fd = float(elem.get('value').replace(',', '.')) * 1e3
            if name == 'freqSlopeConst':
                S = float(elem.get('value').replace(',', '.')) * 1e12
            if name == 'startFreqConst':
                fc = float(elem.get('value').replace(',', '.')) * 1e9
            if name == 'idleTimeConst':
                ti = float(elem.get('value').replace(',', '.')) * 1e-6
            if name == 'rampEndTime':
                tr = float(elem.get('value').replace(',', '.')) * 1e-6
        # Считывание параметров в конфигурации кадра
        for elem in root.iterfind('apiname_frame_cfg/'):
            name = elem.get('name')
            value = float(elem.get('value').replace(',', '.'))
            if name == 'loopCount':
                pls_num = int(elem.get('value').replace(',', '.'))
            if name == 'frameCount':
                frames = int(elem.get('value').replace(',', '.'))
            if name == 'fchirpEndIdx':
                cfg = int(elem.get('value').replace(',', '.'))
                cfg = cfg + 1
        print('Чтение файла с конфигурацией выполнено успешно')
    except:
        print('Отсутствует файл с конфигурацией')
        sys.exit()
    return smp_num, fd, S, fc, ti, tr, pls_num, frames, cfg
#=======================================================================================================


#=======================================================================================================
def twoDimsCFAR_edged(rvacc, num_train, num_guard, fa_rate):
    """
    Локализация целей суммарной карте "дальность-скорость".

    Выполняется с помощью двумерного алгоритма адаптивного определения \
    порога на основе усреднения окна 2D-CFAR (constant false alarm rate).\
    В отличие от twoDimsCFAR на RV-карте не учитываются края шириной 

    Входные параметры:
        1. sfidx - номер субкадра, для которого требуется выполнить \
        локализацию целей
        2. num_train - размеры окна анализа \
        (после вычета защитного интервала)
        3. num_guard - размер окна защитного интервала
        4. fa_rate - значение вероятности ложного обнаружения


    """
    train_Y = num_train[0]
    guard_Y = num_guard[0]
    
    train_X = num_train[1]
    guard_X = num_guard[1]
    
    edge_X = (train_X>>1) + (guard_X>>1)
    edge_Y = (train_Y>>1) + (guard_Y>>1)

    # threshold factor
    trainfactor = np.product(num_train)
    alpha = trainfactor*(fa_rate**(-1/trainfactor) - 1)
    peak_idx = []
    noise = np.array([])

    size_X = rvacc.shape[1]
    size_Y = rvacc.shape[0]
    for vref in range(edge_Y, size_Y - edge_Y):
        for href in range(edge_X, size_X - edge_X):
#            aperture = rvacc[vref - 1:vref + 2, href - 1:href + 2]
#            maxarg = np.unravel_index(np.argmax(aperture), aperture.shape)
#            if (maxarg != (1, 1)):
#                continue

            sum1 = np.sum(rvacc[vref-edge_Y : vref+edge_Y+1, href-edge_X : href+edge_X+1])

            sum2 = np.sum(rvacc[vref - (guard_Y>>1) : vref + (guard_Y>>1) + 1,
                                href - (guard_X>>1) : href + (guard_X>>1) + 1])

            p_noise = (sum1 - sum2) / trainfactor
            threshold = alpha * p_noise
            
            if rvacc[vref, href] > threshold:
                peak_idx.append(np.array([vref, href]))
                noise = np.append(noise, p_noise)

    return np.array(peak_idx, dtype=int)
#=======================================================================================================


#=======================================================================================================
# !!! передавать num_train и num_guard как половины окон
def twoDimsMedianCFAR_edged(rvacc, num_train, num_guard, fa_rate):

    train_Y = num_train[0]
    guard_Y = num_guard[0]
    
    train_X = num_train[1]
    guard_X = num_guard[1]
    
    edge_X = train_X + guard_X
    edge_Y = train_Y + guard_Y

    # threshold factor
    trainfactor = 4*np.product(num_train)
    alpha = trainfactor*(fa_rate**(-1/trainfactor) - 1)
    peak_idx = []
    

    size_X = rvacc.shape[1]
    size_Y = rvacc.shape[0]
    for vref in range(edge_Y, size_Y - edge_Y):
        for rref in range(edge_X, size_X - edge_X):

#            aperture = rvacc[vref - 1:vref + 2, rref - 1:rref + 2]
#            maxarg = np.unravel_index(np.argmax(aperture), aperture.shape)
#            if (maxarg != (1, 1)):
#                continue
            noise = []
            noise.append(np.sum(rvacc[vref-edge_Y : vref-guard_Y, rref-edge_X : rref-guard_X]) / (train_X*train_Y))
            noise.append(np.sum(rvacc[vref-edge_Y : vref-guard_Y, rref-guard_X : rref+guard_X+1]) / ((train_X+1)*train_Y))
            noise.append(np.sum(rvacc[vref-edge_Y : vref-guard_Y, rref+guard_X+1 : rref+edge_X+1]) / (train_X*train_Y))
            
            noise.append(np.sum(rvacc[vref-guard_Y : vref+guard_Y+1, rref-edge_X : rref-guard_X]) / (train_X*(train_Y+1)))
            noise.append(np.sum(rvacc[vref-guard_Y : vref+guard_Y+1, rref+guard_X+1 : rref+edge_X+1]) / (train_X*(train_Y+1)))
            
            noise.append(np.sum(rvacc[vref+guard_Y+1 : vref+edge_Y+1, rref-edge_X : rref-guard_X]) / (train_X*train_Y))
            noise.append(np.sum(rvacc[vref+guard_Y+1 : vref+edge_Y+1, rref-guard_X : rref+guard_X+1]) / ((train_X+1)*train_Y))
            noise.append(np.sum(rvacc[vref+guard_Y+1 : vref+edge_Y+1, rref+guard_X+1 : rref+edge_X+1]) / (train_X*train_Y))
            
            noise.sort()
            threshold = alpha * noise[6]
            
            if rvacc[vref, rref] > threshold:
                peak_idx.append(np.array([vref, rref]))

    return np.array(peak_idx, dtype=int)
#=======================================================================================================
    

#=======================================================================================================
def applyPlotStyle(ax, xlim, ylim):

    ax.set_xlabel("X, м")
    ax.set_ylabel("Y, м")

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    plt.gca().set_aspect('equal', adjustable='box')

    plt.gca().add_patch(mpl.patches.Rectangle((-1, 0),
                                               2, 2, alpha=1.0,
                                               color="orange"))

    ax.xaxis.set_major_locator(mpl.ticker.MultipleLocator(5))
    ax.xaxis.set_minor_locator(mpl.ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(mpl.ticker.MultipleLocator(5))
    ax.yaxis.set_minor_locator(mpl.ticker.MultipleLocator(1))
    plt.grid(which='major')
#=======================================================================================================


#=======================================================================================================
def localMaximaDetector(rvacc, params):
    smp_pad = rvacc.shape[1]
    pls_pad = rvacc.shape[0]
    # параметры для оценки уровня шума
    blockNumR  = 1 << params["blockR"]   # число блоков на которых оценивается уровень шума
    blockSizeR = smp_pad >> params["blockR"]  # размер блока
    blockNumV  = 1 << params["blockV"]
    blockSizeV = pls_pad >> params["blockV"]
    
    pnts = []  # список для записи координат локальных максимумов

    # маска с дальностями, на которых расчитаны пороги по скорости
    v_mask   = np.zeros(smp_pad, int)
    # массив с порогами для каждого профиля скорости
    v_thresh = np.zeros(smp_pad, float)
    # массив с порогами для каждого профиля дальности
    r_thresh = np.zeros(pls_pad, float)
    
    # детектирование на основе RV-карты
    for vel_ref in range(1, pls_pad-1):
        # анализируемый профиль дальности (столбец RV-карты)
        r_prof = rvacc[vel_ref, :]
        
        # оценка уровня шума на профиле дальности
        r_thresh[vel_ref] = threshEstimationMedian(r_prof, blockNumR,
                                                                blockSizeR, params["noiseR"])

        # фильтрация боковых лепестков
        for smp_ref in range(params["minRbin"], params["maxRbin"]):
            # отсчёт является локальным максимумом и его амплитуда больше 
            # порогового значенияна профиле дальности
            if (r_prof[smp_ref] >  r_thresh[vel_ref]) and  \
               (r_prof[smp_ref] >= r_prof[smp_ref-1]) and \
               (r_prof[smp_ref] >= r_prof[smp_ref+1]):
                # оценка уровня шума на профиле скорости
                v_prof = rvacc[:, smp_ref]
                if (v_mask[smp_ref] == 0):
                    v_thresh[smp_ref] = threshEstimationMedian(v_prof, blockNumV, 
                                                                blockSizeV, params["noiseV"])
                    # отмечаем в маске дальность, для которой выполнена оценка уровня шума
                    v_mask[smp_ref] = 1

                # отсчёт является локальным максимумом и его амплитуда больше 
                # порогового значения на профиле дальности
                if (r_prof[smp_ref] > v_thresh[smp_ref]) and  \
                   (r_prof[smp_ref] >= v_prof[vel_ref-1]) and \
                   (r_prof[smp_ref] >= v_prof[vel_ref+1]):
                    pnts.append([vel_ref, smp_ref])
                    
    return np.array(pnts)
#=======================================================================================================
    

#=======================================================================================================
def threshEstimationMedian(prof, blockNum, blockSize, noiseScale):
    block_lvl = []
    for block_ref in range(0, blockNum):
        lvl_temp = np.sum(prof[block_ref*blockSize : (block_ref+1)*blockSize])
        lvl_temp /= blockSize
        block_lvl.append(lvl_temp)
    block_lvl.sort()
    
    return block_lvl[(blockNum>>2) + 1]*noiseScale
#=======================================================================================================
    

#=======================================================================================================
def localMaximaDetectorCFAR(rvacc, num_train, num_guard, fa_rate):
    smp_pad = rvacc.shape[1]
    pls_pad = rvacc.shape[0]
    # параметры для оценки уровня шума
#        train_half_R = self.num_train[1] >> 1
#        train_half_V = self.num_train[0] >> 1
    noise_scaleR = num_train[1]*(fa_rate**(-1/num_train[1]) - 1)
    noise_scaleV = num_train[0]*(fa_rate**(-1/num_train[0]) - 1)
    
    pnts = []  # список для записи координат локальных максимумов
    
    # детектирование на основе RV-карты
    for vel_ref in range(0, pls_pad):
        for rng_ref in range(0, smp_pad):
            rng_right = (rng_ref + 1)%smp_pad
            rng_left  = (rng_ref + smp_pad - 1)%smp_pad
            if (rvacc[vel_ref, rng_ref] >= rvacc[vel_ref, rng_right] and \
                rvacc[vel_ref, rng_ref] >= rvacc[vel_ref, rng_left ]):
                
                r_thresh = threshEstimationCFAR(rng_ref, rvacc[vel_ref, :], 
                                                     num_train[1], num_guard[1], noise_scaleR)
                
                if (rvacc[vel_ref, rng_ref] < r_thresh):
                    continue
                
                vel_right = (vel_ref + 1)%pls_pad
                vel_left  = (vel_ref + pls_pad - 1)%pls_pad
                if (rvacc[vel_ref, rng_ref] >= rvacc[vel_right, rng_ref] and \
                    rvacc[vel_ref, rng_ref] >= rvacc[vel_left,  rng_ref]):
                    
                    v_thresh = threshEstimationCFAR(vel_ref, rvacc[:, rng_ref], 
                                                     num_train[0], num_guard[0], noise_scaleV)
                    
                    if (rvacc[vel_ref, rng_ref] > v_thresh):
                        pnts.append([vel_ref, rng_ref])
                    
    return np.array(pnts)
#=======================================================================================================
    

#=======================================================================================================
def threshEstimationCFAR(smp, prof, num_train, num_guard, noise_scale):

    num_cells = prof.size
    num_train_half = num_train >> 1
    num_guard_half = num_guard >> 1

    noise = 0.0
    for ref in range (0, num_train_half):
        # номера отсчётов с учётом периодичности спектра (всегда в нужном диапазоне значений)
        right_cell = (smp + num_guard_half + 1 + ref) % num_cells
        left_cell  = (smp + num_cells - num_guard_half - 1 - ref) % num_cells
        noise += prof[right_cell] + prof[left_cell]

    return (noise_scale*noise/num_train)
#=======================================================================================================
    

#=======================================================================================================
def azimuth_detector_CFAR(asd_RV, peaks, azm_pad):
    
    pntsRng = peaks[:, 1]
    pntsVel = peaks[:, 0]
    pntsRVA = []
    pnts_num = pntsRng.size
    
    rx_num = asd_RV.shape[1]
    
    for ref in range(pnts_num):
        m = pntsRng[ref]
        k = pntsVel[ref]
        # профиль БПФ
        a_prof = fft(asd_RV[k, :, m] * hamm_win(rx_num), n=azm_pad)
        a_prof = (a_prof.real**2 + a_prof.imag**2)
        a_prof = np.fft.fftshift(a_prof)
               
        # производная профиля
        prof_d = a_prof.copy()
        prof_d[1:] -= a_prof[0:azm_pad-1]
        prof_d[0] = prof_d[1]
    
        # максимумы профиля, превышающие порог CFAR
        for aref in range(0, azm_pad-1):
            if ( (prof_d[aref] >= 0) and (prof_d[aref+1] <= 0) ):
                cfar_thresh = threshEstimationCFAR(aref, a_prof, 12, 8, 20)
                if (a_prof[aref] > cfar_thresh):
                    pntsRVA.append([m, k, aref])
                    
    return np.array(pntsRVA)


#=======================================================================================================


#=======================================================================================================
def azmDetectorSimple(asd_RV, peaks, azm_pad):
    
    pntsRng = peaks[:, 1]
    pntsVel = peaks[:, 0]
    pntsRVA = []
    pnts_num = pntsRng.size
    
    rx_num = asd_RV.shape[1]
    
    for ref in range(pnts_num):
        m = pntsRng[ref]
        k = pntsVel[ref]
        # профиль БПФ
        azimProfileFFT = fft(asd_RV[k, :, m] * hamm_win(rx_num), n=azm_pad)
        azimProfileFFT = (azimProfileFFT.real**2 + azimProfileFFT.imag**2)
        azimProfileFFT = np.fft.fftshift(azimProfileFFT)
        
        # обнаружение по БПФ
        currAzmPnts, h = signal.find_peaks(azimProfileFFT, height=0)
        arr = h['peak_heights']  
        if len(arr) != 0:
            idx = np.argmax(arr)
            currAz = currAzmPnts[idx]
            pntsRVA.append([m, k, currAz])
    
    return np.array(pntsRVA)


#=======================================================================================================
def azmDetectorSimpleEVM(asd_RV, peaks, azm_pad):
    
    pntsRng = peaks[:, 1]
    pntsVel = peaks[:, 0]
    pntsRVA = []
    pnts_num = pntsRng.size
    
    rx_num = asd_RV.shape[2]
    
    for ref in range(pnts_num):
        m = pntsRng[ref]
        k = pntsVel[ref]
        # профиль БПФ
        azimProfileFFT = fft(asd_RV[k, m, :] * hamm_win(rx_num), n=azm_pad)
        azimProfileFFT = (azimProfileFFT.real**2 + azimProfileFFT.imag**2)
        azimProfileFFT = np.fft.fftshift(azimProfileFFT)
        
        # обнаружение по БПФ
        currAzmPnts, h = signal.find_peaks(azimProfileFFT, height=0)
        arr = h['peak_heights']  
        if len(arr) != 0:
            idx = np.argmax(arr)
            currAz = currAzmPnts[idx]
            pntsRVA.append([m, k, currAz])
    
    return np.array(pntsRVA)
#=======================================================================================================
    

#=======================================================================================================
# Функции поиска максимумов на профиле азимутов 
# max_lvl - пороговое значение (во сколько раз максимум должен превосходить среднее значение)
# edge - число неучитываемых отсчётов (по краям) 2*edge < self.abins_num
def azim_dtct_median(asd_RV, peaks, max_lvl, azm_pad):           # median_maxima_finder

    pntsRng = peaks[:, 1]
    pntsVel = peaks[:, 0]
    pntsRVA = []
    pnts_num = pntsRng.size
    rx_num = asd_RV.shape[1]

    for ref in range(pnts_num):
        m = pntsRng[ref]
        k = pntsVel[ref]
        # профиль БПФ
        azimProfileFFT = fft(asd_RV[k, :, m] * hamm_win(rx_num), n=azm_pad)
        azimProfileFFT = (azimProfileFFT.real**2 + azimProfileFFT.imag**2)
        azimProfileFFT = np.fft.fftshift(azimProfileFFT)
        
        # вычисление порогового значения
        azimProfileAcc = np.sum(azimProfileFFT[0:azm_pad])
        profile_max_lvl = azimProfileAcc*max_lvl/float(azm_pad)
        
        # производная профиля
        prof_d = azimProfileFFT.copy()
        prof_d[1:] -= azimProfileFFT[0:azm_pad-1]
        prof_d[0] = prof_d[1]
    
        # максимумы профиля
        for aref in range(0, azm_pad-1):
            if ( (prof_d[aref] >= 0) and (prof_d[aref+1] <= 0) ):
                if (azimProfileFFT[aref] > profile_max_lvl):
                    pntsRVA.append([m, k, aref])
                    
    return np.array(pntsRVA)
#=======================================================================================================
    

#=======================================================================================================
# Функции поиска максимумов на профиле азимутов 
# max_lvl - пороговое значение (во сколько раз максимум должен превосходить среднее значение)
# edge - число неучитываемых отсчётов (по краям) 2*edge < self.abins_num
def azim_dtct_medianEVM(asd_RV, peaks, max_lvl, azm_pad):           # median_maxima_finder

    pntsRng = peaks[:, 1]
    pntsVel = peaks[:, 0]
    pntsRVA = []
    pnts_num = pntsRng.size
    rx_num = asd_RV.shape[2]

    for ref in range(pnts_num):
        m = pntsRng[ref]
        k = pntsVel[ref]
        # профиль БПФ
        azimProfileFFT = fft(asd_RV[k, m, :] * hamm_win(rx_num), n=azm_pad)
        azimProfileFFT = (azimProfileFFT.real**2 + azimProfileFFT.imag**2)
        azimProfileFFT = np.fft.fftshift(azimProfileFFT)
        
        # вычисление порогового значения
        azimProfileAcc = np.sum(azimProfileFFT[0:azm_pad])
        profile_max_lvl = azimProfileAcc*max_lvl/float(azm_pad)
        
        # производная профиля
        prof_d = azimProfileFFT.copy()
        prof_d[1:] -= azimProfileFFT[0:azm_pad-1]
        prof_d[0] = prof_d[1]
    
        # максимумы профиля
        for aref in range(0, azm_pad-1):
            if ( (prof_d[aref] >= 0) and (prof_d[aref+1] <= 0) ):
                if (azimProfileFFT[aref] > profile_max_lvl):
                    pntsRVA.append([m, k, aref])
                    
    return np.array(pntsRVA)
#=======================================================================================================


#=======================================================================================================
def cfar2d(rvacc, num_train, num_guard, fa_rate):
    num_train_half = np.array(np.round(np.array(num_train, dtype=int) / 2),
                              dtype=int)
    num_guard_half = np.array(np.round(np.array(num_guard, dtype=int) / 2),
                              dtype=int)
    num_side = num_train_half + num_guard_half
    # расширяем массив для обнаружения на краях
    num_cells = rvacc.shape
    rvacc = np.concatenate((rvacc[:, num_cells[1] - num_side[1]::],
                            rvacc,
                            rvacc[:, 0:num_side[1]]), axis=1)
    rvacc = np.concatenate((rvacc[num_cells[0] - num_side[0]::, :],
                            rvacc,
                            rvacc[0:num_side[0], :]), axis=0)
    num_cells = rvacc.shape
    # threshold factor
    trainfactor = np.product(num_train)
    alpha = trainfactor*(fa_rate**(-1/trainfactor) - 1)
    peak_idx = []
#    noise = np.array([])

    for i in range(num_side[0], num_cells[0] - num_side[0]):
        for j in range(num_side[1], num_cells[1] - num_side[1]):
            aperture = rvacc[i - 1:i + 2, j - 1:j + 2]
            maxarg = np.unravel_index(np.argmax(aperture), aperture.shape)
            if (maxarg != (1, 1)):
                continue

            sum1 = np.sum(rvacc[i - num_side[0]:i + num_side[0] + 1,
                                j - num_side[1]:j + num_side[1] + 1])

            sum2 = np.sum(rvacc[i - num_guard_half[0]:
                                i + num_guard_half[0] + 1,
                                j - num_guard_half[1]:
                                j + num_guard_half[1] + 1])
            p_noise = (sum1 - sum2) / trainfactor
            threshold = alpha * p_noise
            if rvacc[i, j] > threshold:
                peak_idx.append(np.array([i-num_side[0], j-num_side[1]]))
#                noise = np.append(noise, p_noise)
                
    return np.array(peak_idx, dtype=int)
#=======================================================================================================

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 11:33:21 2022

@author: alex-stend
"""

import numpy as np
import glob
import os
import matplotlib.pyplot as plt
import time
from tqdm import tqdm
# import processingFunctions as pf

base_route = '/home/alex-stend/datasets/'

radar_timestamps = np.loadtxt(base_route + 'bags/radar_31_03.txt')
lidar_timestamps = np.loadtxt(base_route + 'bags/lidar_31_03.txt')

file_bin = base_route + '/bags/radar/31_03.bin'
lidar_frames = sorted(glob.glob(base_route + '/bags/lidar/*.npy'))

def find_nearest(array, value):

    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

cur_fr = 0
real_vels_hist = []

cnt = 0
end_frame = 1

sbfrs  = 3
tx_azm = 4
tx_elv = 5
tx_vel = 1

smp_num = 256
pls_num = 64
pls_num_vf = 128

chp_num = 4
rx_phys = 4      
frm_azm_cs = chp_num*rx_phys*tx_azm*smp_num*pls_num
frm_elv_cs = chp_num*rx_phys*tx_elv*smp_num*pls_num
frm_vel_cs = chp_num*rx_phys*tx_vel*smp_num*pls_num_vf
frm_s  = 2*(frm_azm_cs + frm_elv_cs + frm_vel_cs)
frm_w  = 2*frm_s

for i in tqdm(range(cur_fr, cur_fr + end_frame)):
    
    data = open(file_bin, 'rb')
    position = i*frm_w
    data.seek(position, 0)
    Xt = np.fromfile(data, dtype = np.dtype('int16'), count = frm_s)
        
    Xt_I = Xt[::2]
    Xt_Q = Xt[1::2]
    Xt = Xt_I + 1j*Xt_Q
    
    lidar_idx = find_nearest(lidar_timestamps, radar_timestamps[i] - 0e6)
    lidar_xyz = np.load(lidar_frames[lidar_idx])

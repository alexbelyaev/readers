#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 11:33:21 2022

@author: alex-stend
"""

import numpy as np
import glob
import os
import matplotlib.pyplot as plt
import time
from tqdm import tqdm

base_route = '/home/alex-stend/datasets/new_xsens_datasets/'

radar_timestamp = np.loadtxt(base_route + 'bags/xsens_velocs3.txt')

radar_frames = sorted(glob.glob(base_route + '/bags/radar/*.npy'))
xsens_frames = sorted(glob.glob(base_route + '/bags/xsens/*.npy'))

def find_nearest(array, value):

    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def eulerAnglesToRotationMatrix(translate, theta):

    
    R_x = np.array([[1, 0, 0],
                    [0, np.cos(theta[0]), -np.sin(theta[0])],
                    [0, np.sin(theta[0]), np.cos(theta[0])]
                    ])

    R_y = np.array([[np.cos(theta[1]), 0, np.sin(theta[1])],
                    [0, 1, 0],
                    [-np.sin(theta[1]), 0, np.cos(theta[1])]
                    ])

    R_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
                    [np.sin(theta[2]), np.cos(theta[2]), 0],
                    [0, 0, 1]
                    ])

    R = np.dot(R_z, np.dot(R_y, R_x))
    T = np.array([  [translate[0]],
                    [translate[1]],
                    [translate[2]]
                    ])
    One = np.array([[0],
                    [0],
                    [0],
                    [1]
                    ])
    RT = np.concatenate((R,T), axis=1)
    RT = np.concatenate((RT, One.T))
    return RT

# radar_frame = np.load(base_route + 'bags/radar/00000101.npy')

# fig = plt.figure()
# ax = fig.add_subplot(111)

# plt.plot(-radar_frame[:, 1], radar_frame[:, 0],
#         "ro", markersize=4, alpha=0.9, color="green")
# plt.ylim([0,30])
# plt.xlim([-12.5,12.5])

cur_fr = 20

for i in tqdm(range(cur_fr, cur_fr + 1)):
    radar_pointcloud = np.load(radar_frames[i])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    plt.plot(-radar_pointcloud[:, 1], radar_pointcloud[:, 0],
            "ro", markersize=4, alpha=0.9, color="green")
    plt.ylim([0,30])
    plt.xlim([-12.5,12.5])
    
    for i in range(len(radar_pointcloud)):
        ann_vel = "%.1fм/с" % radar_pointcloud[i][3]
        plt.annotate(ann_vel, (-radar_pointcloud[i][1]+0.5,
                           radar_pointcloud[i][0]+0.5), size=10)
        ann_ampl = '%.2E' % radar_pointcloud[i][4]  
        plt.annotate(ann_ampl, (-radar_pointcloud[i][1]+1.25,
                           radar_pointcloud[i][0]+0.5), size=10)

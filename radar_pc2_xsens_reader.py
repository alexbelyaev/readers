#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 11:33:21 2022

@author: alex-stend
"""

import numpy as np
import glob
import os
import matplotlib.pyplot as plt
import time
from tqdm import tqdm
# import processingFunctions as pf

base_route = '/home/alex-stend/datasets/new_xsens_datasets/'

radar_timestamps = np.loadtxt(base_route + 'bags/xsens_velocs3_radar.txt')
xsens_timestamps = np.loadtxt(base_route + 'bags/xsens_velocs3_xsens.txt')

radar_frames = sorted(glob.glob(base_route + '/bags/radar/*.npy'))
xsens_frames = sorted(glob.glob(base_route + '/bags/xsens/*.npy'))

def find_nearest(array, value):

    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

cur_fr = 50
real_vels_hist = []

cnt = 0
end_frame = 250


for i in tqdm(range(cur_fr, cur_fr + end_frame)):
    radar_pointcloud = np.load(radar_frames[i])
    
    xsens_idx = find_nearest(xsens_timestamps, radar_timestamps[i] - 0e6)
    xsens_xyz = np.load(xsens_frames[xsens_idx])


    if (end_frame == 1):
        fig = plt.figure()
        ax = fig.add_subplot(111)

        plt.plot(-radar_pointcloud[:, 1], radar_pointcloud[:, 0],
                "ro", markersize=4, alpha=0.9, color="green")
        plt.ylim([0,50])
        plt.xlim([-17.5,17.5])

    az_tg = []
    el_tg = []
    
    for i in range(len(radar_pointcloud)):
        # подсчёт азимутов и углов места всех целей на анализируемом кадре
        x = radar_pointcloud[i][0]
        y = radar_pointcloud[i][1]
        z = radar_pointcloud[i][2]
        az_tg.append(np.arctan(y/x)*57)
        el_tg.append(np.arctan(z/x)*57)
        
    real_vels = []
    
    for i in range(len(radar_pointcloud)):
        # фильтрация по дальности
        real_vels.append(radar_pointcloud[i][3]/np.cos(az_tg[i]/57)/np.cos(el_tg[i]/57)
                         + np.sqrt(xsens_xyz[0]**2 + xsens_xyz[1]**2))

        if (end_frame == 1):
            ann_vel = "%.3fм/с" % real_vels[i]
            plt.annotate(ann_vel, (-radar_pointcloud[i][1]+0.5,
                                   radar_pointcloud[i][0]+0.5), size=10)
            ann_ampl = '%.2E' % radar_pointcloud[i][4]  
            plt.annotate(ann_ampl, (-radar_pointcloud[i][1]+1.25,
                                    radar_pointcloud[i][0]+0.5), size=10)
    real_vels_hist.extend(real_vels)

plt.figure()
plt.hist(real_vels_hist, 200)
plt.xlim([-2, 2])

